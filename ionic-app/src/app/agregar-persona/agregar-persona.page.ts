import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PersonasService } from '../services/personas.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { Persona } from '../personas/persona.model';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.page.html',
  styleUrls: ['./agregar-persona.page.scss'],
})
export class AgregarPersonaPage implements OnInit {
  @Input() persona: Persona;
  isEditMode=false;
form: FormGroup;
  constructor(
    private personasService: PersonasService,
     private loadingCtrl: LoadingController,
     private modalCtrl: ModalController
     ) { }

  ngOnInit() {
    this.initAddPersonaForm();

    if(this.persona){
      this.isEditMode=true;
      this.setFormValues();
    }
  }

initAddPersonaForm(){
  this.form =new FormGroup({
    name: new FormControl(null, [Validators.required]),
    price: new FormControl(null, [Validators.required]),
    category: new FormControl(null, [Validators.required]),
    imageUrt: new FormControl(null, [Validators.required]),
    description: new FormControl(null),
  });
}

setFormValues(){
  this.form.setValue({
    name: this.persona.name,
    price: this.persona.price,
    category: this.persona.category,
    imageUrt: this.persona.imageUrt,
    description: this.persona.description
  });
  this.form.updateValueAndValidity();  
}

closeModal(data=null){
this.modalCtrl.dismiss(data);

}

  async submitPersona(){
    const loading = await this.loadingCtrl.create({message: 'Loading ...'});
    loading.present();
    
    let response: Observable<Persona>;

    if (this.isEditMode){
      response = this.personasService.updatePersona(
        this.persona.id,
         this.form.value
         );
    }else{
      response = this.personasService.addPersona(this.form.value);
    }
    
    
    response.pipe(take(1)).subscribe((persona)=> {
      this.form.reset();
      loading.dismiss(); 

      if (this.isEditMode){
        this.closeModal(persona);
      }
    });
  }
}
