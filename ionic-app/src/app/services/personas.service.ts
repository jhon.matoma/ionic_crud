import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Persona } from '../personas/persona.model';

@Injectable({
    providedIn: 'root'
})
export class PersonasService {
    apiUrl='http://localhost:8000/api';
    constructor(private http:HttpClient){}

    getPersonas(): Observable<Persona[]>{
        return this.http.get<Persona[]>(`${this.apiUrl}/personas`);
    }
    addPersona(persona: Persona): Observable<Persona>{
        return this.http.post<Persona>(`${this.apiUrl}/personas`,persona);
    }
    updatePersona(personaId: number, persona: Persona): Observable<Persona>{
        return this.http.put<Persona>(
            `${this.apiUrl}/personas/${personaId}`,
            persona
        );
    }
    deletePersona(personaId: number): Observable<Persona>{
        return this.http.delete<Persona>(`${this.apiUrl}/personas/${personaId}`);
    }
}