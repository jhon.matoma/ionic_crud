import { Component, Input, OnInit } from '@angular/core';
import { Persona } from '../personas/persona.model';
import { ModalController, LoadingController } from '@ionic/angular';
import { AgregarPersonaPage } from '../agregar-persona/agregar-persona.page';
import { PersonasService } from '../services/personas.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  @Input() persona: Persona;

  constructor(private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private personaService: PersonasService) { }

  ngOnInit() {}
  
  closeModal(role = 'edit'){
    this.modalCtrl.dismiss(this.persona, role);
  }

  async openEditModal(){
  const modal = await this.modalCtrl.create({
    component: AgregarPersonaPage,
    componentProps: {persona : this.persona}   
  });
    await modal.present();

    const { data: updatedPersona } = await modal.onDidDismiss();
    if (updatedPersona) {
      this.persona = updatedPersona;
    }
  }

  async onDeletePersona(){
    const loading = await this.loadingCtrl.create({ message: 'Eliminando ...'});
    loading.present();

    this.personaService
    .deletePersona(this.persona.id)
    .pipe(take(1))
    .subscribe(()=>{
      loading.dismiss();
      this.closeModal('delete');
    });
  }
}
