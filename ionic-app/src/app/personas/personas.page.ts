import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import {map, tap} from 'rxjs/operators'
import { PersonasService } from '../services/personas.service';
import { Persona } from './persona.model';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.page.html',
  styleUrls: ['./personas.page.scss'],
})
export class PersonasPage implements OnInit {
  personas$: Observable<Persona[]>;

  constructor(
    private personasService: PersonasService,
     private loadingCtrl:LoadingController,
     private modalCtrl: ModalController
     ) { }

  async ngOnInit() {
  const loading = await this.loadingCtrl.create({message: 'Loading ... '});
  loading.present();

  this.personas$ = this.personasService.getPersonas().pipe(
    tap((personas)=>{
      loading.dismiss();
      return personas;
    })
  );
  }

  async openDetailModal(persona: Persona){
  const modal = await this.modalCtrl.create({
    component: DetailComponent,
    componentProps: { persona },
  });
  await modal.present();

  const { data: updatedPesona, role } = await modal.onDidDismiss();
  if (updatedPesona && role === 'edit') {
    this.personas$ = this.personas$.pipe(
      map((personas)=>{
        personas.forEach(prod=>{
          if (prod.id === updatedPesona.id){
            prod = updatedPesona;
          }
          return prod;
        });
        return personas;
      })
    );
  }
  if (role === 'delete'){
    this.personas$ = this.personas$.pipe(
      map((personas)=>{
        personas.filter(prod => prod.id !== updatedPesona.id);
        return personas;
      })
    );
  }
  }
}
