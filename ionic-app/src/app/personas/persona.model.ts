export interface Persona {
    id: number;
    name: string;
    price: number;
    imageUrt: string;
    category: string;
    description: string;

}