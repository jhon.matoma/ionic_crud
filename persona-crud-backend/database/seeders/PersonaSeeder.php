<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Persona;

class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Persona::create([
            "name"=>"jhon matoma",
            "price"=>1.67,
            "category"=>"desarrollador",
            "description"=>"persona con grande proyeciones",
            "imageUrt"=>"https://us.123rf.com/450wm/2nix/2nix1408/2nix140800144/30818234-perfil-avatar-masculino-uso-de-imagen-para-el-sitio-web-social-vector-.jpg?ver=6"
        ]);

        Persona::create([
            "name"=>"andres sosa",
            "price"=>1.70,
            "category"=>"desarrollador",
            "description"=>"gran desempeño en backend",
            "imageUrt"=>"https://us.123rf.com/450wm/2nix/2nix1408/2nix140800144/30818234-perfil-avatar-masculino-uso-de-imagen-para-el-sitio-web-social-vector-.jpg?ver=6"
        ]);
    }
}
