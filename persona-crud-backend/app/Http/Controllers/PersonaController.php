<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use Faker\Provider\ar_JO\Person;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    public function index(){
        $personas = Persona::all();
        return response()->json($personas);
    }
    public function store(Request $request){
        //validaciones
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'imageUrt' => 'required',
        ]);

        //guardar en base de datos
        $persona =Persona::create([
            'name'=> $request->name,
            'price'=> $request->price,
            'category'=> $request->category,
            'imageUrt'=> $request->imageUrt,
            'description'=> $request->description,
        ]);

        //respuesta de retorno
        return response()->json($persona);
    }

    public function update(Request $request, Persona $persona){
        //validaciones
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'imageUrt' => 'required',
            
        ]);

        //guardar en base de datos
        $persona->update([
            'name'=> $request->name,
            'price'=> $request->price,
            'category'=> $request->category,
            'imageUrt'=> $request->imageUrt,
            'description'=> $request->description,
        ]);

        //respuesta de retorno
        return response()->json($persona);
    }

    public function destroy(Persona $persona){
        $persona->delete();
        return response()->json($persona);
    }
}
